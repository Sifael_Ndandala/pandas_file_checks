
""" 
Code Specification:

This script should contain code that perform data pre-checks on pandas 
dataframe objects. It should have the following functionality:

1. Receive command line argument for checks
    -all should check everything
    -specific list/items of arguments

2. Read in a file provided into a pandas dataframe 
3. Check the file for counts of rows and columns - size
4. Check the file for any duplicates
5. Check the file for missing or nan values against all columns
6. Check the columns for potential mixed data and provide examples

"""

import numpy as np
import pandas as pd 

class DataFramePreCheck:

    def __init__(self, filename=None):
        self.filename = filename
        self.dataframe = None
        if filename.endswith(('.txt', '.csv')):
            self.dataframe = pd.read_csv(filename)
        elif filename.endswith(('.xlsx')):
            self.dataframe = pd.read_excel(filename)
        else:
            try:
                self.dataframe = pd.read_csv(filename)
            except:
                print(f"Couldn't read the file {self.filename}")


    def file_size(self):
        return self.dataframe.shape


    def missing_values(self):
        pass 

    def duplicates(self):
        pass

    
    def __str__(self):
        if self.dataframe:
            return f"{self.dataframe.head()}"
        else:
            return f"Couldn't Read the file {self.filename}\n Please check the following:\n\
                    You can only use .csv and .txt files\n\
                    You can specify delimiters like comma or tabs"


dataframe = DataFramePreCheck(filename='sample.fyt')
print(dataframe.file_size())